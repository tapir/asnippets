package com.tapirgames.util
{
   public class Base64
   {
      public static const Base64Chars:String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

      private static var Base64Char2Index:Array = null;
      private static function GetBase64Char2IndexTable ():Array
      {
         if (Base64Char2Index == null)
         {
            Base64Char2Index = new Array (128); // all char codes in Base64Chars are smaller than 128

            for (var i_char:int = Base64Chars.length - 2; i_char >= 0; -- i_char) // "Base64Chars.length - 2" is to ignore the "=" cahr
            {
               Base64Char2Index [Base64Chars.charCodeAt (i_char)] = i_char;
            }
         }

         return Base64Char2Index;
      }

      public static function DecodeString2ByteArray (text:String):ByteArray
      {
         if (text == null)
         {
            return null;
         }

         var num_chars:int = text.length;
         var num_triples:int = num_chars / 4;
         var num_extras:int = num_chars - num_triples * 4;

         if (num_extras != 0)
         {
            return null;
         }

         var data:ByteArray = new ByteArray ();

         var b0:int, b1:int, b2:int, b3:int;

         var table_char2index:Array = GetBase64Char2IndexTable ();
         var i_char:int = 0;
         data.position = 0;

         while (i_char < num_chars)
         {
            b0 = table_char2index [text.charCodeAt (i_char ++)];
            b1 = table_char2index [text.charCodeAt (i_char ++)];
            b2 = table_char2index [text.charCodeAt (i_char ++)];
            b3 = table_char2index [text.charCodeAt (i_char ++)];

            data.writeByte ((b0 << 2) | ((b1 & 0x30) >> 4));
            data.writeByte (((b1 & 0x0f) << 4) | ((b2 & 0x3c) >> 2));
            data.writeByte (((b2 & 0x03) << 6) | b3);
         }

         data.position = 0;
         return data;
      }

      public static function EncodeByteArray2String(data:ByteArray):String
      {
         if (data == null)
         {
            return null;
         }

         var num_triples:int = data.length / 3;
         var num_extras:int = data.length - num_triples * 3;

         var num_chars:int = (num_extras > 0 ? num_triples + 1 : num_triples) * 4;
         var char_indexes:ByteArray = new ByteArray ();
         char_indexes.length = num_chars;

         var b0:int, b1:int, b2:int;

         data.position = 0;
         char_indexes.length = 0;

         for (var i_triple:int = 0; i_triple < num_triples; ++ i_triple)
         {
            b0 = data.readByte () & 0xFF; // "& 0xFF" is essential for negative values
            b1 = data.readByte () & 0xFF;
            b2 = data.readByte () & 0xFF;

            char_indexes.writeByte ((b0 & 0xFC) >> 2);
            char_indexes.writeByte (((b0 & 0x03) << 4) | (b1 >> 4));
            char_indexes.writeByte (((b1 & 0x0F) << 2) | (b2 >> 6));
            char_indexes.writeByte (b2 & 0x3F);
         }

         if (num_extras > 0)
         {
            b0 = data.readByte () & 0xFF; // "& 0xFF" is essential for negative values

            if (num_extras == 2)
            {
               b1 = data.readByte () & 0xFF; // "& 0xFF" is essential for negative values

               char_indexes.writeByte ((b0 & 0xFC) >> 2);
               char_indexes.writeByte (((b0 & 0x03) << 4) | (b1 >> 4));
               char_indexes.writeByte ((b1 & 0x0F) << 2);
               char_indexes.writeByte (64); // "="
            }
            else // num_extras == 1
            {
               char_indexes.writeByte ((b0 & 0xFC) >> 2);
               char_indexes.writeByte ((b0 & 0x03) << 4);
               char_indexes.writeByte (64); // "="
               char_indexes.writeByte (64); // "="
            }
         }

         var char_index:int;

         for (var i_char:int = 0; i_char < num_chars; ++ i_char)
         {
            char_index = char_indexes [i_char];
            char_indexes [i_char] = Base64Chars.charCodeAt(char_index);
         }

         char_indexes.position = 0;
         return char_indexes.readUTFBytes (num_chars);
      }
   }
}
